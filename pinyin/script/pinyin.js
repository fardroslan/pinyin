jQuery(window).ready(function(){

	var api_url = "https://translation.googleapis.com/language/translate/v2";
	var api_key = "AIzaSyDUPGXqTB34_fQHhXFXuhivKv_2c6r9c0Q"; //restrict usage
	var source = "en";
	var target = "zh";
	var keyword = "";
	var keyword_match = "";

	function onError(data, status){}

	function onSuccess_pinyin2chinese(data, status){
		if(data != ""){
			$(data.data.translations).each(function(index, value){
				console.log(value);
				keyword_match = value.translatedText;
				$('#result').show();
			});
		}else keyword_match = "";
		$(".chinesetranslate").text(keyword_match);
	}

	function pinyin2chinese(keyword){
		if(keyword != ""){
			var url = api_url + "?key=" + api_key + "&source=" + source + "&target=" + target + "&q=" + keyword;
			$.ajax({
				type: "get",
  				dataType: "json",
  				url: url,
  				cache: false,
  				success: onSuccess_pinyin2chinese,
				error: onError
			});
		}else{
			keyword_match = "";
			$(".chinesetranslate").text(keyword_match);
		}
	}
	
	function search_content_by_keyword(keyword_match){
		//continue content search (from database) no execution at the moment. 
		//dummy return demo purpose:-
		
		
	}

	$('#pinyin_textbox').donetyping(function(){
		$('#result').hide();
		pinyin2chinese($(this).val());
	});

});

(function($){
    $.fn.extend({
        donetyping: function(callback,timeout){
            timeout = timeout || 1e3; // 1 second default timeout
            var timeoutReference,
                doneTyping = function(el){
                    if (!timeoutReference) return;
                    timeoutReference = null;
                    callback.call(el);
                };
            return this.each(function(i,el){
                var $el = $(el);
                $el.is(':input') && $el.on('keyup keypress',function(e){
                    if (e.type=='keyup' && e.keyCode!=8) return;
                    if (timeoutReference) clearTimeout(timeoutReference);
                    timeoutReference = setTimeout(function(){
                        doneTyping(el);
                    }, timeout);
                }).on('blur',function(){
                    doneTyping(el);
                });
            });
        }
    });
})(jQuery);